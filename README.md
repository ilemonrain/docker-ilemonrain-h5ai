## 0. 关于镜像

!> **此镜像目前仅仅是功能集成，未做任何安全性配置！**  
**如果需要进入镜像内部 (Linux Bash Shell)，请参考下面的教程！**


## 1. 镜像说明

此镜像基于 ilemonrain/lamp 作为母镜像，去除MySQL(要你有何用)，极大程度精简了镜像体积，使得镜像拉取速度更快，占用资源更低。到目前为止。此镜像已经集成：  
- 一套LAP环境，包括：  
  - L：Alpine Linux 3.7.0  
  - A：Apache 2.4.29  
  - P：PHP 7.1.15  
- h5ai 0.29.0 (已集成在Docker镜像中)  
- Zip/Unzip (用于压缩包处理)  
- (*)PHP-GD扩展 (用于显示照片缩略图)  
- (*)PHP-EXIF扩展 (用于显示照片信息)  
- (*)FFmpeg (用于显示视频缩略图)  
- (*)Imagemagick (用于显示PDF缩略图)  

(*)：这些功能仅包含于 ilemonrain/h5ai:full 镜像中。

## 2. 使用方法

启动命令行：  
```
docker run [-t/-d] -p [80]:80 -v [$PWD]:/h5ai --name h5ai ilemonrain/h5ai:[lite/full]
```
**[-t/-d] ：**决定是以后台运行模式启动或是前台监控模式启动。
 - 使用-d参数启动，镜像将不会输出任何日志到你的Console，直接以Daemon模式启动。Deamon模式启动下，可以使用docker logs h5ai令显示启动日志。  
 - 使用-t参数启动，将会直接Attach你的镜像到你的Console，这个模式启动下，你可以直观的看到镜像的启动过程，适合于初次部署镜像，以及镜像Debug部署使用。你可以使用Ctrl+C将Docker镜像转入后台运行，使用docker attach h5ai命令显示启动日志。  

**-p [80]:80：** h5ai on Docker 需要映射的端口，方括号中端口可任意修改为你需要的端口  

**-v $PWD:/h5ai：** 映射目录，将会自动在选定的目录下创建h5ai程序目录(_h5ai)和Apache2必要的.htaccess文件，如果在在使用完成后不需要这两个文件，可以自行删除；如果需要映射当前目录(可以使用pwd命令确定)，请直接输入 “$PWD”  

**--name h5ai：** Docker容器的名称，可以自行修改  

**ilemonrain/h5ai:[lite/full]：** 启动的镜像名称，请注意：如果你只是为了测试镜像，或者Docker宿主机所在网络环境不佳，请使用lite分支 (即 ilemonrain/h5ai , ilemonrain/h5ai:latest , ilemonrain/h5ai:lite 均可)；正式使用或者需要完整功能，请使用full分支 (ilemonrain/h5ai:full)  

## 3. 进入Docker Bash Shell
有的时候需要进入Docker镜像的Bash Shell进行修改，只需执行以下命令：
```
docker exec -it h5ai sh
```

## 4. Bugfix的记录以及感谢
 - (2018/03/27) 修复了启动过程中，unzip配置参数错误，导致第二次启动时出现提示确认覆盖，发生异常的错误 (感谢 十六君)

## 5. 联系作者 & 求打赏！
企鹅：942109647 (加好友请注明 **Docker h5ai 技术交流** ，不带任何注明直接拒加好友)
Email：ilemonrain#ilemonrain.com
Telegram：[@ilemonrain](https://t.me/ilemonrain) (不会24小时在线，有事情请发E-mail)
打赏通道 (吃土少年求帮助！)：请点击下面的赞赏支持~喵~